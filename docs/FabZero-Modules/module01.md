# 1. GitLab

Lors de ce module nous avons appris à créer notre site web, pour cela nous sommes passés par gitlab en utilisant un nouveau type de language. Il y a eu beaucoup d'essais erreurs.

## Installation Git
Tout d'abord pour utiliser gitlab, il était plus simple d'utiliser le logiciel depuis notre propre ordinateur, pour ce faire il a fallu faire une série de procédure pour l'utiliser. Nous avons besoin d'utiliser le terminal de notre ordinateur, nous avons donc commencé par installer [bash](https://www.gnu.org/software/bash/), c'est un  interpréteur en ligne de commande, cependant il ne s'exécutait pas sur mon ordinateur, j'ai suivi un tutoriel qui me demandait de télécharger Ubuntu, ce qui était une très mauvaise idée car je suis devenue une invitée de mon propre ordinateur et c'est pourquoi en procédant au processus pour avoir accès à ma clef SSH publique je n'ai su l'avoir. J'ai donc suivi un autre procédé pour résoudre le problème.
## Procédure d'installation
 Je suis donc repartie de 0. Au lieu d'utiliser bash car il ne créait pas de raccourci sur mon ordinateur, j'utilise [Windows Powershell](https://fr.wikipedia.org/wiki/Windows_PowerShell) qui est un invite de commande. 
 Ensuite il a fallu connecter mon Pc à GitLab, nous avons donc eu besoin de la clef SSH
 - Créer la clef SSH sur mon pc avec la commande suivante: `ssh-keygen -t ed25519 -C "<comment>"`
- Il demande où placer le dossier .ssh, je laisse la suggestion qui est `/home/<user>/.ssh` avec <user> qui est mon nom utilisateur.
- Après avoir créé la clef, je me dirige dans ce dossier pour récupérer la clef dans le fichier .pub
- Après avoir récupéré la clef, je l’ajoute dans mes clefs ssh sur gitlab

![](../images/GITLAB.jpg)

Puis il a fallu cloner le répertoire git sur mon pc : Maintenant que mon pc est authentifié avec mon compte gitlab, je peux cloner en ssh le projet ou je le souhaite sur mon pc.
- git clone

Après avoir cloné le projet, j’ai besoin d’un éditeur de texte approprié pour travailler dessus. J’ai choisi [Visual Studio Code](https://code.visualstudio.com/). Pour m’accompagner dans la gestion du git, j’utilise [GitHub Desktop](https://desktop.github.com/) qui m’accompagne avec une interface graphique pour ainsi mettre à jour mon avancement sur le serveur git au lieu de procéder à chaque fois depuis mon treminal, ce qui est une méthode beaucoup plus rapide. Après avoir fait ma documentation sur Visual Studio Code, j'enregistre cette dernière et je passe sur GitHub desktop, il me montre les modifications apportées : il suffit de nommer le commit  et de cliquer sur *Commit to main* en bas à gauche, ensuite il faut envoyer ces modifications vers le git et donc cliquer sur le bouton *Push origin* au centre de l'écran. Et c'est ainsi que je *pull* et *push* vers le serveur en ligne.

![](../images/powershell.jpg)

## Ajouter et compresser image
Il est important de réduire la résolution de l'image car les informations uploadées sont conservées dans les serveurs à jamais, ce qui contribue à la pollution. Pour ce faire on peut passer dans des logiciels tels que [Gimp](https://www.gimp.org/). 
- ouvrir le fichier dans gimp, aller dans échelle d'image, et reduire le nombre de pixels
- ensuite exporter l'image et reduire la qualité de celle-ci, pour ma part je l'ai mise à 50%

Ensuite dans l'éditeur de texte j'ai ajouté l'image dans le fichier image et encodé dans la partie texte ![ ] (image/nomdufichier.jpg) (sans aucun espace).

## Liens utiles

- [Visual Studio Code](https://code.visualstudio.com/)
- [github desktop](https://desktop.github.com/)
- [Gimp](https://www.gimp.org/)






