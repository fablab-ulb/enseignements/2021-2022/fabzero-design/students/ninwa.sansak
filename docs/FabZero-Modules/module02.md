# 2. Conception assistée par ordinateur : Fusion 360 

Lors de ce module, nous avons appris à utiliser le logiciel [Fusion 360](https://www.autodesk.com/products/fusion-360/personal) pour nous permettre de modéliser en 3D des objets. Pour prendre en main le logiciel et entamer le travail de design, nous avons modélisé notre objet de référence. Cela nous permet de bien comprendre comment celui ci est conçu.
 

## Modélisation
### Objet et mesures

Mon objet étant une table aux dimensions cubiques, cela était rélativement simple de la redessiner et comprendre son fonctionnement. La table est constituée de 4 pieds de 15 mm et de tirants de même dimensions qui renforcent sa stabilité au niveau du bas des pieds mais également du haut. Il n'y avait donc que 3 "morceaux" à dessiner. Le pied, le tirant et la table.

  ![](../images/table.jpg)

### Outils 

Je vais passer rapidement en revue comment Fusion 360 fonctionne. Pour créer un objet en 3D il faut passer par l'étape 2D, avec une forme de base on peut créer le volume. C'est pourquoi sur FUsion360 on doit passer par l'étape *esquisse* (premier icone dans la barre d'outils), on choisit le plan de travail et une fois l'esquisse terminé on clique sur le bouton vert *terminer esquisse* en haut à droite pour retourner dans la modélisation 3D. Créer un objet dans fusion consiste a faire principalement des allers-retours entre l'esquisse et la 3D.

![](../images/m2outils.jpg)

Ensuite je vais indiquer où trouver et l'utilité des outils principaux utilisés. 
- *esquisse* : comme expliquer plus tôt, c'est l'outil principal.
- *extrusion* : à partir d'une esquisse 2D on peut donner un volume  à l'objet en l'*extrudant*, icône juste à côté de esquisse.
- *réseau* : dans l'onglet *créer*, en bas il y a *reseau* il faut chosir le type de réseau, dans ce cas c'est un reseau rectangulaire, et ensuite choisir le nombre de rangée par direction
- *copier* et *déplacer* : pour *copier* il suffit juste de faire un clique droit, ou alors selectionner l'objet et faire ctrl+C et ctrl+V, et ensuite le déplacer avec l'outil *déplacer* qui est la flèche en croit noir dans la section *modifier*.
### Réalisation sur Fusion 360
Il m'a d'abord fallu créer un *paramètre* sur Fusion 360. Un *paramètre* est une donnée qu'on encode au préalable, puis pour une mesure X on donne sa dimension selon le nom du paramètre, et ainsi lorsqu'on effectue un changement de paramètre toutes les mesures qu'on avait mises selon ce paramètre changent. Pour mon objet, les paramètres mis sont la hauteur de la table et la longueur de la tablette.

![](../images/m2onglet.jpg)

![](../images/m2paramètres.jpg)
-


Puis il a fallu dessiner le modèle. Le procédé fut comme suit :
1. Dessiner les pieds : 
 - dessiner le premier pied dans esquisse
 - extruder celui ci à hauteur 500 mm moins l'épaisseur de la tablette (30mm) 
 - faire un réseau cubique 2x2 pour faire les 4 pieds.

 ![](../images/m2pieds.jpg)

 2. Dessiner les tirants
 - retourner dans esquisse et dessiner la forme du tirant de même dimension que le pied sur une des faces orientée à l'intérieur du cube à une hauteur de 2mm.
 - extruder le tirant jusqu'au pied opposé.
 - refaire le même procédé que pour les pieds.
 - une fois les 4 tirants du bas faits, les copier et les placer au niveau du haut.

 ![](../images/m2tirants.jpg)

 3. Dessiner la tablette 

Il n'a plus que fallu créer la tablette, j'ai dessiné la forme dans l'esquisse puis j'ai extrudé en précisant la hauteur à laquelle je souhaitais la placer, à 500 mm c'est à dire à la hauteur paramétrée et son épaisseur.

![](../images/m2tablette.jpg) 

Cependant, la table est composée d'un ensemble, une table plus réduite qui peut se placer dans la plus grande. Elle est de même proportion, au lieu de 500 mm elle est de 450.
J'ai donc copié le modèle et en ai réduit les proprtions, mais celle-ci possède une tablette au niveau des tirants du bas, j'en ai donc ajouté une en procédant exactement de la même manière que celle du haut mais avec la bonne hauteur.
De plus pour pouvoir les emboiter, il faut qu'elles glissent l'une dans l'autre, il y a donc logiquement un tirant qui bloque, j'ai donc retiré un tirant de la table la plus grande.

![](../images/m2modélisation3D.jpg)

## Liens utiles

- [Fusion 360](https://www.autodesk.com/products/fusion-360/personal)
- [Modèle format STL](../files/Table.stl)
- [Modèle fichier fusion 360](../files/table.f3d)






