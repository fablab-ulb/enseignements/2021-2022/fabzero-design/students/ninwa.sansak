# 3. Impression 3D

Cette semaine nous avons appris à utiliser les imprimantes 3D mises à disposition au Fablab. Pour cet exercice, il nous a fallu imprimer notre objet dessiné au module 2 sur Fusion 360 pour une impression qui dure moins de 2 heures.

## Prusa
  Tout d'abord, il nous a fallu télécharger le logiciel [Prusa](https://www.prusa3d.com/prusaslicer/)  adapté aux machines du FabLab : PRUSA i3 MK3 et i3 MK3S/MK3S+. Ensuite, une fois notre fichier Fusion 360 terminé, il faut l'exporter en fichier STL et le glisser dans le logiciel PrusaSlicer. L'objet apparaitra et il faudra le mettre à l'échelle souhaitée selon les capacités de la machine (verifiez donc les dimensions maximales de la machine)
### Préréglages 
Nous sommes donc sur la plateforme PrusaSlicer, la première chose à faire est de la mettre en mode "Expert". Il est bon de savoir que selon la forme de l'objet il est nécessaire ou non de mettre des supports pour la bonne impression de celui-ci, car pour être imprimé le filament doit être imprimé sur une surface plane et non dans le vide (du moins jusqu'à un certain angle). Il est donc essentiel de reflechir au bon positionnement de l'objet pour minimiser la quantité de support et donc minimiser le temps d'impression et de matériaux. De plus, partir avec une base fine qui monte haut n'est egalement pas l'idéal, il faudra en plus rajouter une bordure à la base, ce qui augmente encore plus le temps d'impression. Dans ce cas ci, il était judicieux de placer l'objet avec le coté plat sur la plaque chauffante.

![](../images/m3prusa.jpg)

### Parametrages
Il faut régler les paramètres en fonction de notre utilisation, les paramètres que j'ai moi-même modifié ont un cadenas orange à côté, sinon ce sont les paramètres par défaut. :
- *Couches et perimètres* : la hauteur de le couche dépend de la buse, il est préférable de mettre la hauteur de la couche à 50%, dans ce cas ci la buse est de 4mm, la hauteur de la couche est donc paramétrée pour 2mm.
- *Remplissage* : le remplissage a été mis à 15% ce qui reste convenable pour un objet de cette taille. Il est conseillé de ne pas dépasser 25% de remplissage car au dela, cela ne change pas tellement par rapport à 100%, et cela consomme une quantité de matière inutile. De plus, pour minimiser le temps d'impression, le remplissage était plus efficace en *Gyroïde*.
- *Supports* : comme dit précédement, il fut nécéssaire de mettre des supports à cause des tirants présents sur la table. Ensuite, vu que les supports doivent être retirés sans abimer l'objet, il est préférable de les faire assez large. Dans ce cas, l'espacement du motif et du motif d'interface ont été réglés respactivement à 2 et 1 mm. Ce sont des données à configurer selon l'objet.

Concernant la vitesse, celle ci sera modifiée directement depuis l'imprimante.



![](../images/m3reglage.jpg)

Une fois tous les réglages choisis, cliquez sur *découper maintenant* pour voir ce que l'impression va engendrer en terme de temps et de coût. Il faut ensuite convertir le fichier en G-Code qui sera transféré sur une carte SD et directement placée sur l'imprimante.
### Préparation impressions

Pour garantir au mieux la réussite de l'impression, il est nécéssaire de bien préparer la machine.

- Fermer les fenêtres pour éviter les courants d'air.
- Nettoyer le plateau avec de l'acétone et retirer tous les residus
- Il faut vérifier qu'il ait suffisement de filement dans la bobine ou de changer de couleur si on le souhaite. Dans mon cas j'ai changé de couleur en procédant comme suit : aller dans menu, cliquer sur *unload*, attendre que la température soit atteinte et retirer le filement. Pour la bobine actuelle il faut faire attention à coincer le bout pour éviter les noeuds. Ensuite on place la nouvelle bobine, on decoupe le bout si nécéssaire pour avoir quelque chose de net, on retourne dans menu et on clique sur *load*, le filement va se fondre avec la couleur de la bobine precédente, il faut répéter cette opération jusqu'a ce que la couleur soit la bonne en cliquant sur *No* lorsque la machine demandera si cela est convenable.


- Ouvrir le fichier contenu dans la carte SD.

## Premier essai

J'ai donc ouvert mon fichier et le temps d'impression était estimé à 1h53. Je suis partie d'une vitesse de 100%, je suis restée à côté de l'impression durant les premières couches pour verifier que tout allait bien. Par la suite j'ai pu augmenter le temps d'impression par tranche de 50%, j'ai atteint une vitesse de 200%. Le temps final de l'impression est donc passé à 1h28.

Mon objet était prêt, il n'a plus que fallu retirer les supports. **Cependant un problème est apparu**, lors du retrait des supports, les pieds de la table était trop fins et donc assez fragile, **ils se sont détachés de l'objet ou se sont cassés**.

![](../images/m3essai1.jpg)

## Deuxième essai

**Le premier essais ayant échoué à cause des pieds trop fins**, il a fallu retourner dans le fichier fusion 360 pour en augmenter leur épaisseur. J'ai refait tout le procédé d'exportation jusqu'au logiciel PrusaSlicer. Cependant le temps d'impression était éstimé à 3h. J'ai réduit la densité du remplissage à 10% et le motif en *triangle*, le temps est passé à 2h46. Le reste étant déjà optimisé, le solution trouvé a été de reduire l'échelle de l'objet à 8%, et ainsi le temps est passé à 1h50.

![](../images/m3reglages2.png)

J'ai procédé comme le premier essai et j'ai augmenté le temps d'impression par tranche pour attendre une vitesse de 270%, pour atteindre au final un temps d'impression de 1h21.

![](../images/m3essai2.jpg)




## Useful links

- [Prusa](https://www.prusa3d.com/prusaslicer/)
- [Modèle format STL](../files/Table.stl)
- [Modèle fichier fusion 360](../files/table.f3d)





