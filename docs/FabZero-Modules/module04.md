# 4. Découpe assistée par ordinateur

Cette semaine nous avons appris à utiliser la découpeuse laser. Cet apprentissage se divise en deux phases : la première en groupe pour appréhender la machine, et la seconde individuelle qui consiste à créer une lampe qui s'inspire des caractéristiques de notre objet.

## Précautions 
Avant  de lancer une découpe il est important de connaitre certaines précautions en fonction des machines disponibles pour garantir une sécurité. Au FabLab il existe 2 types de machines : la Laseraur et la Epilog Fusion pro 32. Les 2 machines ne traitent que les dessins vectoriels.

### Précautions
- Connaitre avec certitude quel matériau est utilisé 
- Toujours ouvrir la vanne d'air comprimé
- Toujour allumer l'extracteur de fumée 
- Savoir où est le bouton d'arrêt d'urgence
- Savoir où trouver un extincteur de CO2 

### Matériaux pour la découpe

**Matériaux recommandés**

Bois contreplaqué (plywood / multiplex)
Acrylique (PMMA / Plexiglass)
Papier, carton
Textiles


**Matériaux déconseillés**

MDF : fumée épaisse, et très nocive à long terme
ABS, PS : fond facilement, fumée nocive
PE, PET, PP : fond facilement
Composites à base de fibres : poussières très nocives
Métaux : impossible à découper


**Matériaux interdits !**

PVC : fumée acide, et très nocive
Cuivre : réfléchit totalement le LASER
Téflon (PTFE) : fumée acide, et très nocive
Résine phénolique, époxy : fumée très nocive
Vinyl, simili-cuir : peut contenir de la chlorine
Cuir animal : juste à cause de l'odeur
### Machines

#### *Laseraur* 

**Spécifications**
- Surface de découpe : 122x61 cm
- Hauteur maximum : 12 cm 
- Vitesse maximum : 6000mm/min
- Puissance Laser : 100 W 
- Type de laser  : Tube CO2 (infrarouge)



**Format pris en charge**
  Fichier SVG (plus efficace), Fichier DXF (plus difficile), DBA (Format spécifique à la Laseraur)

  [Voici le lien vers son guide d'utilisation](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md)

#### *Epilog Fusion Pro 32* 

 **Spécifications** 
 - Surface de découpe : 81 x 50 cm
 - Hauteur maximum : 31 cm
- Puissance du LASER : 60 W
- Type de LASER : Tube CO2 (infrarouge)

**Format pris en charge** 

SVG (si pas en SVG, on peut l'importer dans Inskape pour l'importer en SVG)

[Voici le lien vers son guide d'utilisation](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/EpilogFusion.md)

## S'exercer au calibrage 

La consigne de cette phase consite à caractériser la focalisation, la puissance, la vitesse, la largeur de trait, etc. de votre découpeuse laser pour le pliage et la découpe de polypropylène (PP). Cela nous permet de nous familiariser avec la machine. On a utilisé Epilog fusion pro 32, on a donc procédé comme suit : 

Pour configurer la découpe, j'attribue à chaque type de découpe une couleur qui auront chacune leur vitesse et leur puissance spécifique, cela nous permet de voir comment le matériau réagit en fonction de ces paramètres. Il est assez ensentielle de mettre les découpes dans l'orde, c'est-à-dire faire les formes intérieures en premier et le contour qui détache la forme en dernier, sinon le matériau risque de bouger.

![](../images/m4calibrage_découpe.jpg)

Après avoir calibré les découpes en suivant la graduation, comme par exemple le carré rouge qui a une vitesse de 10% pour une puissance de 4 %. Pour l'*Auto Focus* il faut selectrionner **Plunger** et selectionner **Vector** pour le *Process Type*, **Engrave** c'est pour les découpe matricielle. On voit donc qu'en fonction de la vitesse et de la puissance la matériau se découpe ou non, et avec une certaine netteté.

![](../images/m4découpe.jpg)

Nous avons également procédé à des tests pour le pliage des matériaux. On suit le même procédé de couleur, mais cette fois ci on garde la même vitesse de 75% et nous avons modifié uniquement la puissance qui va de 10 à 55 %.

 ![](../images/m4calibrage_pliage.png)

![](../images/m4pliage.jpg)

## Conception de la lampe 
 L'exercice suivant consiste à concevoir et fabriquer de manière individuelle une ou plusieurs lampes à partir d’une feuille de Polypropylène translucide (50 x 80 cm) en partant du thème de l’objet choisi. Le moins de chute possible, pas de colle, pas de vis, ne rien ajouter à la lampe.

 ### Choix de sa forme
 Pour réaliser mon luminaire, j'ai pensé à une utilisation pratique pour mon objet. Celui ci étant une table à dessin, je me suis dit qu'il serait fort utile une lampe qui puisse se poser aisément sur le coin de la table, qui n'encombre pas et dont la source lumineuse peut être rapprochée ou éloignée selon les besoins.

### Essais papier

 Tout d'abord je suis partie de la forme carré de la table. j'ai fait des tests de pliage pour avoir un faisceau dirigé, je l'ai incliné pour que ce faisceau soit bien dirigé. Pour tenter de le faire tenir en oblique j'ai d'abord tenté d'y mettre des pieds, mais finalement je me suis rendu compte qu'en ajuster la longueur du tube et du pliage posé sur la table, la lampe pouvait tenir d'elle-même sans ajout de renfort supplémentaire, en plus pour une stabilité optimale, le source lumineuse placée à l'intérieur sert de contre point, ce qui **renforce à nouveau sa stabilité**.

 ![](../images/m4idée.jpg)

  La lampe lorsqu'elle est assemblée, est fermée du côté bas et ouverte par le haut, en format papier elle pouvait être retournée pour avoir un faisceau beaucoup plus dirigée, toutefois, en polypropylène la matériau étant plus lourd, la charge était **déséquilibrée**, et la source lumineuse n'était pas assez lourde pour faire un contre poids suffisant, c'est donc une position qui n'a pas été gardée pour le resultat final.

![](../images/m4papier1.jpg) ![](../images/m4papier2.jpg)

  Ensuite, Je me suis rendu compte qu'en dépliant ou en resserant la lampe on pouvait également modifier le faisceau lumineux et la forme de l'objet. Les petites "pinces" métalliques de la source aident au maintient de la lampe.

![](../images/m4papier3.jpg)


  ### Découpage

J'ai dessiné mon fichier destiné à la découpe sur Autocad en créant bien différents calques en fonction des découpes souhaitées. Ici, il y a 3 types de découpes différentes. Cependant sur AutoCad les fichiers ne peuvent être enregistrés qu'en DXF, donc en fonction de la machine utilisée il faudra le convertir en SVG ou non. [Voici le fichier autocad](../files/lampe.dwg).
Sur une planche j'ai pu placer 2 lampes de tailles normales et 3 de plus petites tailles où des plus petites soucres sans branchements peuvent êtres placées. la quantité de déchets est donc minime.

![](../images/m4autocad.jpg)

Pour découper sur Epilog Fusion les étapes à suivre sont : 

- Exporter le fichier en DXF depuis AutoCad
- Ouvrir la découpeuse laser
- Ouvrir le fichier dans Inkscape déjà préinstallé sur les ordinateurs du Fablab ou télécharger le [ici](https://inkscape.org/fr/release/inkscape-0.92.4/).
- Réduire les lignes  à 0,01 mm pour avoir les lignes les plus fines possibles
- Cliquer sur le bouton Imprimer et attendre l'ouverture du Dashboard Epilog.
- Choisir **PLunger** dans *Auto Focus*
- Choisir **Vector** dans *Process Type* (pour chaque ligne découpée)
- Calibrer les différentes découpes : pour les découpes (en rouge) j'ai choisi une vitesse de 10% pour une puissance de 40%, pour les pliages primaires (lignes mauves) j'ai choisi une  vitesse de 70 pour une puissance de 55, et pour les pliages secondaires (lignes oranges) j'ai choisi une vitesse de 70 pour une puissance de 50.
- Placer l'image souhaitée sur l'écran. Pour ce cas-ci il faut bien placer l'image sur le bord droit car la découpe prend la quasi totalite de la planche. 
- Bien placer les lignes roses, tout ce qui sera en dehors du cadre rose ne sera pas découpé.
- Quand tout est okay appuyer sur *Print*
- Le fichier sera envoyé sur l'écran de la machine, le selectionner.
- Allumer l'aspirateur et appuyer sur *Play*
- Attendre quelques seconde que la poussière s'évacue et récuper la planche

### Resultat 
![](../images/m4lampe.jpg) 

## Useful links

- [Guide Laseraur](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md)
- [Guide Epilog Fusion 32](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/EpilogFusion.md)
- [Fichier calibrage découpe](../files/calibration_découpe.svg)
- [Fichier calibragepliage](../files/calibration_pliage.svg)
- [Fichier lampe](../files/lampe.dwg)
- [Inkscape](https://inkscape.org/fr/release/inkscape-0.92.4/)

