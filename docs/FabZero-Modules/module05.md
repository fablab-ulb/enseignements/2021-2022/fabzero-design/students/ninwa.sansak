# 5. Usinage assisté par ordinateur

Cette semaine nous avons appris  à utiliser [Shaper](https://www.shapertools.com/fr-fr/#), c'est un outil portatif qui nous assiste lors de la découpe, on la déplace et elle coupe exactement là où on le souhaite. Voici une [vidéo](https://youtu.be/Xo0CiJjTGJE) explicative de comment elle fonctionne. 

![](../images/m5shaper.jpg) 

## Précautions 
Il y a queulques précautions à respecter avant d'utiliser la machine pour **garantir une sécurité et éviter tout blessé**.

- Utiliser la machine sur un plan de travail stable 
- Être dans une position stable et en équilibre (ne pas s'étendre pour couper)
- Pour la découpe : fixer le matériau sur le plan de travail pour éviter qu'il ne saute une fois coupé.
- Toujours relever la fraiseuse (bouton rouge) avant de passer d'une forme à une autre
- Toujours utiliser un aspirateur de poussières 
- Porter une tenue appropriée, pour que rien en s'emmêle à la machine.
- Il y a une poignée à l'arrière pour déplacer la machine.



## Fonctionnement machine
 La machine a une découpe maximal de 43 mm de profondeur, cette dimension peut être réduite si on utilise une fraise assez petite. Elle peut couper du bois, les métaux non ferreux, et le plexiglass, mais il est déconseilé de découper ce dernier, la machine n'est pas la mieux adaptée.


Le fichier utilisé doit être en SVG, il est important d'avoir son dessin en vectoriel, on peut utiliser le logiciel Inscape.
### ShaperTape
 L'inconvenient de cette machine c'est qu'elle ne peut être utilisée qu'avec du ShaperTape, car elle fonctionne et se repère avec une caméra : on place le ShaperTape pour que la caméra le repère et sache où couper. Avant de lancer la découpe il faut tous les "scanner" pour établir le plan de travail. Lorsque la machnine repère le tape, celui ci devient bleu à l'écran. Il est important de bien orienter la caméra par rapport au tape car si la machine ne voit plus le ShaperTape et donc le plan de travail elle s'arrête car elle se repère pas et ne sait donc pas où couper. Sur la partie en haut à droite, un logo qui représente le tape s'affiche en rouge lorsqu'elle ne le repère plus, et en noir lorsque tout est ok. L'autre inconvenient majeur est qu'on perd une partie du matériau qui devient non utilisble car perdue pour placer le ShaperTape. 

![](../images/m5shapertape.jpg) 
![](../images/m5ecran.jpg)

### Fraise
Il existe plusieurs taille de fraise qu'on peut utiliser selon nos besoins, elles sont assez fragiles il faut faire attention à ne pas les faire tomber.
On peut changer la taille de la fraise de découpe en fonction de l'utilisation souhaitée :
 - Débrancher le mandrin SM1 sur la machine
 - Dévisser avec une clé et retirer le compartiment (le mandrin) avec la fraise
 - Desserrer l'écrou de vérouillage avec une clé à molette en appuyant sur le bouton de blocage
 - Changer la fraise en ne l'enfonçant pas trop
 - Resserrer en appyant toujours sur le bouton de verrouillage.
### Vitesse de fraisage
Il existe 6 vitesse de fraisage. Selon les test effectués la vitesse 1 n'est pas assez rapide et la découpe n'est pas droite, et le 6 est trop rapide ce qui brûle le bois. Il faut ainsi faire des test de ce qui est le mieux adapté en fonction de notre propre vitesse à nous.
### Découpe
 
 Avant de lancer la découpe, il est utile de savoir qu'il existe plusieurs types de déoupes, elles se règlent directement sur la Shaper, pour faire la *Outside* il est nécéssaire d'avoir une forme fermée.

  ![](../images/m5découpes.jpg)

On peut lancer la découpe : 
- On insère la clé USB, depuis l'écran on place le dessin pùsouhaité et on règle l'échelle.
- Cliquer sur *fraise* et choisir la profondeur de découpe 
- Choisir le type de découpe
- Appuyer Sur le bouton *On* et allumer l'aspirateur
- Lancer la découpe 

Des flèches montrent les directions qu'on doit suivre, et sur l'écran il y a une zone qui doit être placée la découpe, si ces indications ne sont pas respectées, la fraiseuse se relève. Lorsqu'une zone est découpée, elle sera mise en bleu sur l'écran et sera gardée en mémoire.

Lorsqu'on passe d'une forme à une autre, il faut relever la machine et la replacer. On peut faire une pause en appuyant sur le bonton orange sur la poignée de gauche, et la relancer en appuyant sur le bouton vert sur la poignée de droite.

![](../images/m5resultat.jpg)


## Liens utiles

- [Shaper](https://www.shapertools.com/fr-fr/#)
- [Vidéo commennt elle fonctionne ](https://youtu.be/Xo0CiJjTGJE)
- [Vidéo d'utilisation](https://www.youtube.com/watch?v=DekAjAOIVvQ&ab_channel=Shaper)


