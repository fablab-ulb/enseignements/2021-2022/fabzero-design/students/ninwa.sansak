# Final Project
Bienvenue sur la documentation concernant  la conception du projet final, ici vous pourrez voir le processus de conception ainsi que les phases de réflexions entre le choix de la fonction et le projet final.

## Design or not design

Avant de commencer l'élaboration du projet final, nous avons eu un cours d'introduction sur *ce qu'était le design*, et comment l'aborder. Pour ainsi avoir les étapes utiles pour nous permettre d'avoir les bonnes clefs.

Tout d'abord, il est essentiel de différencier le *signe* de la *fonction*, entre comment **l'objet est représenté et son utilité**. On  comprend donc que le Design est une question de fonction. Les objets peuvent arriver à un stade où ils deviennent leur propre carricature : ils ont l'air design mais n'ont aucune utilité, c'est ce qu'on cherche à éviter pour notre projet.

En resumé, les objets sont classifables en deux parties : 
- *l'outils*
- *le signe*

Les 2 concepts cohabitent ensemble dans un objet design. Cependant pour notre exercice nous allons nous concentrer  sur la question de *l'outil*.
Pour ce faire nous avons **6 étapes** par lesquelles il est important de passer pour aboutir à un objet concret : 

- Déconstruire la fonction de l'objet
- Résoudre un vrai problème
- Comprendre les dimensions spécifiques
- Esquisse/ Essais-erreurs/ Améliorations
- Adaptation, diffusion édition
- Nom et pictogramme

Ces étapes seront chacunes explicitées pa chapitre plus bas.

# **Processus de conception**

Je vais donc vous expliquer les étapes et le processus par lequel je suis passé pour arriver à mon projet final qui est une table à dessin modulable

##  **Etape 1** : Déconstruction de la fonction

La première étape  consiste à comprendre et analyser l'objet, son utilité et son problème, **déconstruire sa fonction**. Nous devons juste être observateur, uniquement regarder l'objet à travers sa fonction et ainsi apprendre  **à voir l'objet autrement**.

Par groupe de 3, nous avons observé et analysé les objets de chacuns des autres membres du groupe, ce qui nous a permis de voir notre objet à travers d'autres prismes, et voir des choses que nous n'aurions peut-être pas vu.

Pour mon objet, la table à dessin utilisée plus spécifiquement pour peindre, nous en avons conclu que ses fonctions étaient essentiellement donc de pouvoir peindre mais également y faire d'autres utilisation comme manger, travailler, y poser des objets etc. La grande différence entre les tables à dessin et une table lambda est la fait qu'on puisse **adapter** l'angle de la tablette pour mieux y travailler et avoir un travail plus efficace, de plus ajouter la spécificité de la table poour peindre et pas juste de dessin engendre de prendre en compte toute une série d'objet utile et utilisés dans ce cadre. Ce sont ces points qui ont été mis en évidence pour le réalisation de la table.

## **Etape 2** : Résoudre un vrai problème 

La deuxième étape consiste à connaîtres les contraintes et les limites de l'objet. On apprend à rentrer dans l'objet pour y voir ses faiblesses dans les objets déjà existants et ainsi créer un objet qui répond à un vrai problème car ce sont les contraintes qui font l'objet.

Comme expliqué plus tôt, la contrainte principale est **l'adaptabilité de la tablette**, mais j'ai essayé d'aller plus loin en me disant que malgré que l'angle soit déjà un point essentiel pour le confort, la hauteur de la table n'est pas adaptée à tous les autres humains ni même à **toutes les postures**, la table actuelle est pensée pour être accompagnée de la posture classique d'*assis sur une chaise*, pourtant ce n'est clairment pas la posture la plus confortable pour tout le monde. C'est donc une des limites de l'objet que je souhaite résoudre.

Toutfois, je tiens à préciser qu'ici je vais me concentrer sur les positions plutôt *basses*, pour me consacrer à un panel précis, travailler debout relève d'autres outils de travails comme le chevalet, et ce n'est pas ce qui m'interesse ici.

Ensuite, une table à dessin est souvent **encombrante et difficile à ranger**, c'est pourquoi la rendre polyvalente peut déjà aider en partie à ce qu'elle ne fasse pas perdre de place pour une utilisation non quotidienne. Avoir une taille réduite limite aussi sa perte de place, il faudrait éviter d'en avoir une ni trop grande ni trop petite.

De plus, un point important est comment resoudre le problème des **objets qui risquent de tomber** avec une tablette inclinée ? C'est un point crucial pour le confort de l'utilisateur.

Enfin, la dernière contrainte est celle de la **stabilité de la table**, elle doit être stable avec son poids propre mais également le poids des objets qu'on y pose et/ou le poids d'une personne qui s'y accoude ou même qui s'y assoie.

En conclusion, la définition de cet objet est de créer une table modulable et adaptable à toutes les positions.

## **Etape 3** : Comprendre les dimensions spécifiques

Ici, il s'agit de penser en ergonome, il faut penser aux positions du corps par rapport à l'objet pour avoir un confort d'utilisation. Les dimensions accompagnent le dessin car ce sont les positions qui font la forme de l'objet. Il est important de penser aux gestes et aux poids des choses.

Il y a 3 dimensions essentielles pour concevoir cette table :
- La position du corps qui influe sur la hauteur de la table : *hauteur et positions*
-  Les dimensions et le poids de l'objet qui doivent être facilement déplaçable et peu encombrante : *Poids et Dimensions*
- Le plan de travail qui doit s'incliner et l'accesibilité des objets : *Adaptabilité et accesibilité*

### **Hauteur et position** 
La hauteur et les positions sont étroitements liées: la hauteur de la tablette influe selon la position dans laquelle l'utilisateur est. Il est important de rappeler que la hauteur confortable idéale est lorsque celle ci arrive au niveau des coudes : en moyenne elle varie autour de 75 cm, ce qui ne convient ni aux personnes de grandes tailles ni de petites tailles.

![](./images/projet/hpersonne.jpg)

Mais qu'en est il lorsqu'on veut changer de position après une journée assis sur une chaise ? On est contraint de rester assis de cette manière car notre support de travail ne peut s'adapter à nos besoins. Comme dit précédement la hauteur du plan de travail est lorsque celui ci arrive à hauteur de coude. Par exemple pour ma part en mesurant 1m58, la hauteur idéale :
 - assise sur une chaise : **70 cm**
 - sur les genoux : **55 cm**
 - accroupie : **43 cm**
- en tailleur : **35 cm**

![](./images/projet/positions.jpg)

Un autre point, un détail certes mais crucial également, et de penser où mettre nos jambes, il ne doit pas y avoir d'obstacle si on souhaite étendre nos jambes ou les croiser, ce qui est clairement un frein au confort de l'utilisateur.


### **Poids et Dimensions**

 Un des but premier de cette table est qu'elle soit facilement utilisable, déplaçable et donc peu encombrante, il y a deux point à ajuster pour y arriver : le *poids* de la table et ses *dimensions*.
 Le poids doit permettre de la soulever aisement pour éviter de l'abandonner dans un coin **(poids ?????)** 
Sa dimensions doit permettre d'être assez grande pour avoir un plan de travail confortable mais pas trop grande pour qu'elle ne soit pas encombrante, la dimensions la plus confortable serait de 80 sur 60 cm, ce qui equivaut plus au moins à la taille d'une feuille A1. Ces tailles se justifient par le fait que 60 cm est la dimensions maximum à laquelle le bras peut s'étendre pour attraper un objet, et 80 cm est assez convenable pour poser un support et s'accoudoir des deux bras. De plus, ces dimensions permettent de passer l'objet par n'importe quelle porte.

![](./images/projet/dimensions.jpg)

### **Adaptabilité et Accesibilité**

Lorsque la position est choisie, il y a deux données cruciales à prendre en compte pour améliorer le confort : l'angle du plan de travail en premier lieu, ce qui engendre une surface non horizontale et donc un souci au niveau des objets qu'on souhaite y disposer. Il faut donc résoudre cette question des objets : quels sont les types d'objets que nous pourrions y retrouver ? un pot d'eau, des pinceaux, des crayons et feutres, le plateau de peinture, les feuilles ou les toiles, des mouchoirs etc etc. Tant d'objet où la stabilisation est importante.



## **Etape 4** : Esquisses

Désormais, il s'agit de créer des prototypes de nos idées et concept jusqu'à l'obtention d'un résultat qui répond à nos contraintes. Pour rappel, ici l'objet final souhaité est une table compacte destinée à peindre qui s'adapte à toutes hauteurs et positions.

## Premières idées
Plusieurs idées me sont venues à l'esprit, mais un concept général m'a semblée être le mieux adapaté pour répondre aux contraintes, je vous le présente à travers plusieurs points: 

* *Hauteurs et angles* : Après plusieurs reflexions, je me suis dit qu'une solution pour adapter la hauteur et l'angle de travail était de combiner la modulabilité des 2 ensembles. Je m'explique : en créant 4 pieds où la hauteur de chacune est modulable indépendamment des autres, on peut augmenter la hauteur des deux pieds arrières pour surelever la partie arrière et ainsi créer un plan de travail oblique.

![](./images/projet/pieds.jpg)
 
 Pour ce faire, le pied de la table se ferait en 3 parties : 2 qui s'emboitent entre elles et qui definissent 2 à 4 hauteurs principales, ces encochent peuvent être définies avant le découpage des pièces en fonction de la hauteur de chaque personne. Dans ce cas ci, la hauteur sera défini en fonction de ma propre taille. Ensuite la troisième pièce est modifiable en angle pour ajuster la hauteur parfaitement au centimètre près.

 ![](./images/projet/S6.jpg) 

 
 * *Facile de rangement* : Permettre de ranger la table plus facilement et qu'elle soit plus compacte et transportable, pour ce faire l'idéal serait de détacher les pieds de la table avec un système d'emboitement entre les pieds de la table et la plan de travail.
 
 ![](./images/projet/S7.jpg)

 * *Plan de travail* : Cependant créer une surface qui s'emboite, peut engendrer une surface pas parfaitement lisse à la surface du plan de travail, ce qui est absolument pas pratique pour une surface où on est censé dessiner. La solution serait de créer une encoche et non un trou au niveau de la planche de travail pour que le pied puisse y rentrer, par conséquent cela permet de ne pas toucher à la surface de travail.

![](./images/projet/S8.jpg)

 * *Stabilité* : Concernant la resistance et la stabilité de la table, il est important de déjà savoir en quel matériau elle sera, dans ce cas ci elle sera en bois, car c'est un matériau qui se trouve facilement, selon le bois il peut se trouver à des prixs très abordable, et selon le bois choisi l'esthetique se modifie. Donc, pour sa stabilité, l'idée était de créer une surface de contact plus large avec le plan de travail, et comme celui ci sera découpé sur une planche en bois, il s'agira de créer une tête de pieds avec deux planches qui s'entrecroisent et formeraient une croix, ce qui permettrait de résister aux forcent (mouvement) qui viennent de tout sens. De plus, cette forme permet d'avoir une meilleure résistance quant à la répartition des charges sur la table.
 

![](./images/projet/S9.jpg)

## En pratique

Tout ceci n'est que croquis et hypothèse, maintenant il s'agit de créer un prototype et réaliser cette idée. Comme dit précemment le matériau qui sera utilisé pour réaliser cette table sera le bois.

#### **Croquis 1** 
j'ai rassemblé toutes ces idées en 1 seul objet, en commençant par le pied de la table, et j'ai réalisé un premier croquis. Après avoir pensé et dessiné les différentes pièces, je me suis rendu compte que beaucoup de détails posaient problème
- Premièrement la pièce qui sert au contreventement est en plein milieu de la pièce, et donc l'autre pièce qui doit se déplacer verticalement se retrouve bloquée.
- De plus, les 2 pièces princpales du pieds ne sont pas réellement tenues entre elles, il aurait fallu trouver une solution pour qu'elles soient rattachées et emboitées l'une à l'autre.
- Ensuite la dernière partie du pieds, la plus fine, est bien trop fine et fragile pour être stable et retenir le poids de la table.

![](./images/projet/baseS1.jpg)

#### **Croquis 2**

Pour ce dessin j'ai essayé de régler les problèmes précédents. Comme on peut le voir le pieds se composent de 6 pièces en bois et 2 pièces imprimées. La base du pied est composée d'une section plus importante, et les 2 parties supérieures sont maintenues entre elles. Et la partie pour le contreventement est décalée sur le coté et emboitée à la fois au plan de travail et au pied, ainsi elle n'encombre plus les mouvements verticaux permettant l'extension du pied.

![](./images/projet/baseS2.jpg)

![](./images/projet/dessincad.jpg)

 Les pièces sont maintenues entre elles par un système de vis et d'écrous, qui peut être sérré/dessérré pour maintenir les pièces entre elles. Cette partie est imprimée en 3D (Cependant pendant la réalisation du prototype les machines ont eu un souci et ces pièces n'ont pas pu être imprimées, je n'ai donc pas pu tester encore si ce système fonctionnait réellement)

 ![](./images/projet/écrou.jpg) ![](./images/projet/vis.jpg)

 Il est à noter que j'ai défini les encoches en fonction de mes propres dimensions, pour une autre personne, il suffit de replacer les encoches selon les dimensions de chaque personne et augmenter les dimensiosn de certaines pièces.


 **Réalisation**


Pour cette première réalisation on peut voir que le système de hauteur fonctionne relativement bien. Le système de vis et écrou maintient bien les différentes parties, cependant il est à vérifier avec les 4 pieds et la plan de travail réunies avec un poids dessus, pour vérifier s'ils résistent réellement. Mais on peut constater quand même quelques problèmes et quelques améliorations à apporter :

![](./images/projet/test1.jpg)

- Tout d'abord, comme dit dans un des points précédent, les 4 pieds peuvent être ajustés séparemment pour ainsi definir l'angle de plan de travail souhaité. Cependant, avec les encoches créées, la hauteur maximum à laquelle peut aller la table est de 68 cm dans ce cas ci. Il sera donc compliqué d'avoir un angle de travail, (donc en augmentant la hauteur des pieds arrières) en position assise sur une chaise.

- La base du pied n'a pas suffisemment de plat pour maintenir le poid du reste, il est à noter que le bois utiliser est du bois récupérer et donc son épaisseur n'est pas l'épaisseur souhaité, dans ce cas ci elle est trop fine, ce qui joue beaucoup dans la stabilité du pied. Mais c'est donc une information à prendre en compte, avoir un bois assez épais et un pied suffisamment large. De plus les parties supérieures sont beaucoup trop large par rapport au pied inférieur, il y a une disproportion entre les éléments, ce qui augmente ce désiquilibre.

- Ensuite, j'ai eu un problème pour passer d'une encoche à une autre, je n'avais pas assez de marge pour déplacer la "cale" qui est entre les deux planches du milieu qui peuvent se mouvoir verticalement hors de l'encoche et faire glisser cette partie du pied pour l'ajuster vers l'autre encoche. Elle est donc resté bloquée.

![](./images/projet/cale.jpg)

- Ensuite la première partie du pied est trop longue, je n'ai pas pu la faire descendre jusqu'au bout pour atteindre la première encoche, car la base du pied bloquait, c'est juste un problème de dimensionnement des éléments.

![](./images/projet/blocage.jpg)

Cependant, il me reste encore à régler la partie concerant les objets, une question fondamentale.

# **Retour à zéro**

Après discussion, le projet tel que présenté actuellement est compliqué d'utilisation, la taille des pieds et son utilisation est trop importante par rapport à la taille du plan de travail, et le changement de taille n'est pas fluide. J'ai donc voulu simplifier et rendre plus efficace le changement de hauteurs et en ayant une structure plus simple. Je suis donc repartie chercher des exemples de structure en bois et d'autres modulables. 

## Exemples

J'ai trouvé quelques exemples de table en bois, mais également certains support d'ordinateur, qui répondaient à mes attentes : une structure faite entièrement en bois, resistante et stable, bien-sûr.

![](./images/projet2/exemples.jpg)

J'ai voulu combiner la rapidité de changement de hauteur des suppors d'ordinateur à une strcuture plus importante. En gardant une table peu encombrante et peu lourde. 

## Esquisse/Esquisse

Pour cette esquisse je me suis d'abord concentré sur l'adaptabilité de la hauteur, quand cette étape sera fonctionnelle j'intégrerai la partie de l'angle du plan de travail et de la stabilité des objets. 

J'ai donc simplifié le changement de hauteur en une seule étape : il y a 2 pieds avec chacun un seul changement. Il suffit de retirer le "blocage" de part et d'autres, de faire glisser les 2 parties entre elles jusqu'au niveau souhaité, et de replacer le blocage. La hauteur maximum correspond à la position assise pour ma taille, c'est à dire aux alentours de 68cm, et la partie la plus basse correspond à la position assise en tailleur.

![](./images/projet2/principe.jpg)

Puis il suffit d'ajouter les élèments utiles à la stabilité de la table. J'ai essayé d'avoir le minimum de matière, mais cette partie est encore en cours de réflexion pour voir si elle peut être plus efficace.

![](./images/projet2/3D.jpg)

![](./images/projet2/plan.jpg)

Avant de tester ceci en objet réel et de perdre de la matière, je l'ai d'abord réalisé en maquette pour voir si la structure tenait. Malgré qu'il manquait un élément utile à la stabilité et conçu avec une matière relativement fine et fragile, la maquette a pu resister au poids d'un livre épais. Ce qui est prometteur pour la réaliser à l'echelle avec un matériau plus dense. 

![](./images/projet2/maquette.jpg)

Cependant, tant que je ne l'ai pas réalisé en taille réelle et avec les bonnes attaches, je ne peux confirmer que la table sera complétement stable. Et étant donné que le FabLab n'ouvre qu'a partir de mardi, et vu le coût des matériaux, je ne pourrais en faire l'expérience qu'une seule fois avant le pré-jury.

## Réalisation

J'ai réalisé la découpe à partir de la [Shaper Origin](https://www.shapertools.com/fr-fr/#). Etant donné que cette machine fait perdre une partie du matériau, j'ai voulu en économiser en plaçant les différentes pièces sur 2 grandes plaques, mais ce n'était pas la meilleure solution, parceque comme il y avait beaucoup de ligne de découpe, je découpais par dessus le shaper tape et à certains endroits la shaper ne détectait plus le shaper tape, et j'ai donc dû finir à la scie sauteuse.

![](./images/projet2/découpe.jpg)

![](./images/projet2/scie.jpg)



De plus, certaines formes n'étaient pas jointes, j'ai du mal fermer certaines, ce qui donc avec la shaper ne m'a pas permis d'utiliser la fonction découpe "intérieure" ou "extérieure" pour toutes, il y aura donc un problème de raccord ou de dimensions entre certaines pièces. Un point important à bien prendre en compte à la prochaine réalisation. 

Ensuite, je maitrise encore mal la Shaper, par manque de temps j'ai voulu aller plus rapidement, mais j'allais beaucoup trop vite, les découpes ne sont donc pas nettes.

Concernant les blocages, je les ai imprimées avec les imprimantes 3D. J'en avais besoin que de 2, mais j'en ai imprimé 4 au cas où un renforcement serait nécéssaire. En accélérant le temps, elles se sont imprimées en moins de 2h


![](./images/projet2/attaches.jpg)

![](./images/projet2/pièces.jpg)

### **Résultat**

![](./images/projet2/haut.jpg)

![](./images/projet2/Bas.jpg)

Voici le résultat final. Dans l'ensemble la structure est relativement stable, à quelques défauts près. Je l'ai rélaisé à partir de plaque en bois multiplex de 15 mm. La table est facielemnt montable et démontable, en 2 min elle peut être montée, elle n'est pas trop lourde égalment, donc pour la transporter cela est bénéfique. Les problèmes constatéés sont :

tout d'abord dû aux problèmes rencontrés avec la machine, certaines encoches et trous ne sont pas dans les bonnes dimensions, comme les encoches verticales qui acceuillent la planche du milieu, qui stabilise la plus la structure, sont trop grandes et donc n'englobent pas entièrement la planche, ce qui la rend peu stabe.

Ensuite, je n'ai pas correctement stabilisé la partie basse des pieds entre elles, donc la partie du centre n'est pas correctement maintenue et donc lorsuq'on bouge lattéralement la table elle bouge un peu. Il faudra donc trouver une solution pour bien maintenir la partie du milieu où les 3 pièces principales du pieds se rejoignent.

De plus, comme j'ai dû finir certaines pièces avec d'autres outils, les découpes et les placements n'étaient pas très précis donc le changement de hauteur n'est pas très fluide. 

Un autre point important, et pas des moindres, lorsqu'il faut descendre la hauteur, ce n'est pas aussi rapide que je pensais, le fait que l'attache doive traverser 3 planches, ça rend la chose plus complexe, on prend du temps à la faire traverser à travers les trous, et en plus on doit maintenir le poids de la table pendant qu'on essaie de bien la placer. C'est un point crucial à améliorer.

Pour conclure, si le problème du maintient des pieds est coorectement fait et découpé, la table est relativement stable et ne devrait plus bouger. Elle resiste également à un poids (plusieurs outils assez lourd). Je l'ai secoué dans tous les sens et je l'ai faite retombé plusieurs fois de quelques dizaines de centimètres et elle est toujours debout et assez stable.

![](./images/projet2/test.jpg)

## Préjury 

Nous avons passé un préjury qui nous a permis de passer une étape pour notre objet. Le jury s'est déroulé toute la journée, et les membres du jury étaient répartis en 5 groupe de 2 à 3 personnes. Cela nous a permis d'avoir des points de vues différents et plus de remarques pernitantes que si nous avions eu qu'un seul jury. Je vais donc détailler les remarques reçues que je vais prendre en compte pour avancer dans la conception de mon objet qui je n'avais pas déjà évoqué dans les points précédent.

1. Le premier point sur lequel je dois me concentrer est de bien préciser ce que je veux et ce que je veux en réalisant cette table, donc de bien définir les fonctions en partant d'un storytelling pour couvrir tous les aspects d'une problématique.

2. Ensuite, un autre aspect qui doit être réfléchi, est le language de ma table, elle est assez "brouillon", avec beaucoup de languages différents ainsi de que type de pièces, et en particulier travailler sur des accroches plus propres.

3. De plus, la légereté de la table et la quantité de matière et de pièce utilisée peut être réduite, comme par exemple, les planches intermédiaires peuvent être découpées dans des plaques plus fines, pour alléger visuellement et physquement la table.

Finalement, on a constaté un autre aspect qui peut être exploitée, le fait qu'elle peut être utilisée pour les enfants, et ainsi combinée son utilité
 

# Développement & Avancement

Suite à ce jury, j'ai poussé mes réflexions concerant cette table.
 - Premièrement j'ai pensé à la **circulation autour** de la table pour qu'elle ne soit ni encombrante lorsqu'elle est à la hauteur la plus haute ni à la hauteur la plus basse, lorsqu'elle est à la hauteur la plus basse, on doit pouvoir s'y assoier sans encombre ou obstacle. De plus on doit pouvoir la remonter et l'abaisser facilement, que ce soit pour un adulete ou un enfant. Cela rejoint l'idée de *storytelling*, réflechir à la manière dont la table est utilisée, et ce dont il est question en fonction des besoins.

- Ensuite, le point concernant le fait qu'elle soit qu'elle puisse être **utilisée par les enfants** m'a semblé très pertinent. Donc été voir les hauteurs pour les enfants en fonction des tranches d'ages et elles correspondaient relativement bien aux hauteurs que j'avais emises pour mon cas, j'ai donc établie ces hauteurs là qui **vont des enfants de basses sections à une personne de 1m90** assise, le tout pouvant être utilisé à des positions différentes : cm 44, 49, 54, 59, 64, 69, 74, 80.

- Un autre point sur lequel je souhaitais travailler est de **réduire** la quantité de **matière utilisée**, tout en gardant une stabilité maximum. Car réduire la matière permettrait non seulement de réduire les coûts, mais également de réduire le poids de l'objet, ce qui peut faciliter son utilisation par un enfant.

 - De plus, en rapport avec le point précédent, plus dans une question de conception, il me faut penser aux proprtions des différentes pièces, avec qu'elle soit le moins encombrante possible (toujours dans cette logique de réduire la matière), mais le plus stable possible également. Mais cela sera fait dans les essais à échelle réduite.

## Redéfinition

En résumé avec tous ces points d'analyse, je reviens pour mieux définir la définition de mon objet :**Une table compact destinée à la peinture et au dessin, utilisable pour tout le monde, et peu cher.**

La définition récouvre beaucoup de points, ce qui est pas mal ambitieux.

## Tests

 J'ai redessiné l'objet sous plusieurs angles, je les ai donc réalisés à plus petite échelle pour en voir les défauts et ce qui peut être améliorer avant de la réaliser à grandes échelles. ( ! Le tape est là pour solidifier les attaches entre les pièces du pieds qui ne tenaient pas, à cause d'une mauvaise conception des raccords de ma part, d'où l'importance de les tester avant en taille réelle. !)

  1. Test 1
  

  ![](./images/final/test1.jpg)


   Pour le test 1, j'ai repris la même structure que mon objet précédent en y ajusant les proportions et pour observer comment se mouvoir autour correctement.
   Les points positifs : en ayant ajusté les dimensionnement, à la position la plus basse il n'y a pas la partie des pieds qui dépassent à l'arrière, car avec la vesrion précédente lorsque je faisais descendre la table, elle allait trop à l'avant donc l'espace nécessaire pour utilser la table e-était plus important. De plus, de cette manière lorsque la table est basse, et que l'espace est déjà assez réduit pour s'y installer, il n'y a pas d'encombre pour s'y installer correctement

   ![](./images/final/schémas.jpg)

   Cependant elle comporte plusieurs défauts : elle est un peu moins stable lorsque je la malmène latéralement. Elle est relativement stable à vide, mais une fois du poids dessus, elle est moins endurante, elle bouge un peu particulièrment quand elle est dans sa position la plus haute, mais mon chat de 4 kg s'est assise dessus et elle ne s'est pas brisée ou effondrée c'est quand même un point encourageant. En outre, avec la pièce de bois qui est au niveau du pied bas, elle bloque la chaise. Ce qui peut vite être encombrant pour ranger le mobilier ou pour s'y assoier confortablement par terre.

  2.  Test 2

  ![](./images/final/test2.jpg)

Ce test était pour solidifier la table lorsqu'on y pose du poids importants. Les points positifs : elle est plus stable latéralement, mais toujours pas optimal, et elle est plus facile à faire descendre car il y a ces pièces obliques qui servent de poignée. Cependant, je ne sais pas le faire descendre complétement à cause de la pièce qui reuni les 2 parties du pieds, et elle n'est toujours pas la plus stable avec du poids. En outre, avec ces pièces obliques le passage sous la table n'est pas très pratique, et il y a toujours la planche intermédiaire qui bloque le passage de la chaise.

Cet essai n'est donc pas concluant.

  3. Test 3

  ![](./images/final/test3.jpg)

Ce test est relativement stable, étant donné qu'il n'y a plus la pièce intermédiare qui bloque le passage de la chaise ou des jambes 

  4. Test 4
  
  ![](./images/final/test1.jpg)

  Ce test recouvre les mêmes points que le 3, mais cependant elle est beaucoup moins stable, ce qui est dommage car elle utilisait moins de matière.

5. Ce point était dédié aux tests pour l'angle du plan de travail que je devais réalisé,  mais ayant eu le covid je n'ai plus pu sortir et donc avoir accès au fablab, ce point sera donc directement exposé dans l'objet final. Et donc je pratirai de l'hypothèse que le système fonctionne.


 ### Conclusion

En conclusion, le test le plus concluant est le test numéro 4, certains points sont encore à améliorer. J'étais partie pour réaliser encore 1 essais ou 2 sur les proportions et l'épaisseur des planches pour voir si je peux la rendre encore plus légère, j'avais donc acheté le bois et réalisé les croquis nécessaire à sa réalisation, en même temps que les essais 5, mais comme dit précedemment, problème de covid. Je vais donc directement passer au dessin final.

# Dessin final

Maintenant que la structure finale a été choisi, il est important de faire des tests uniquement zoomés sur les raccords, les détails, et les emboitements pour vérifier que tout fonctionne et ne pas perdre de matière. Je ne peux donc pas les réaliser, donc tout ceci sera tout à fait **hypothétique**, et je partirai du principe qu'ils **fonctionnent correctements** et qu'ils sont bien dessinés, jusqu'à sa véritable réalisation.

![](./images/final/général.jpg)


## Détails et raccords



### **Jonctions des pieces**
La jonction des pièces se fait par un assemblage facilement détachable qui n'a pas besoin de vis pour être maintenue, pour ainsi réduire les matériaux nécessaire. Ce système fonctionne par un système de *pièces* à 3 embouts : les 2 parties aux extrémités se *pincent* pour être insérées dans les trous respetifs et puis se relachent pour ainsi se bloquer et maintenir la pièce de bois entrelacée dans l'autre. C système permet donc de monter et démonter la table aisement sans aucun autre outils.

![](./images/final/raccords.jpg)

### **Système d'angle**

Qu'est ce que serait une table à dessin sans une tablette modulable ? J'ai donc fait en sorte qu'on puisse ajuster l'angle de travaille, je propose 6 angles différents : d'abord à plat, un support à l'horizontal. Ensuite selon mes recherches, il est dit que l'idéal en théorie serait d'avoir un support à 90°, mais  en pratique ce n'est pas du tout pratique, j'ai donc fait un plan à 66° pour qu'il soit suffisemment vertical mais confortable à la fois. Ensuite il est également dit que le plus confrotable est d'avoir des angles entre 20 et 45°  voici pourquoi j'ai inséré comme angle de travail des angles de 20, 31, et 44°, ainsi qu'un angle de 56° comme intermédiaire. Mais tout ceci n'est que personel, et chacun à son angle de confort, j'ai essayé d'avoir ici quelque chose de plus universel qui répond au plus grand monde.

De plus, pour facilter le rangement, une sous tablette faite avec un matériau léger pour ne pas alourdir la structure est placée sous la tablette de travail, qui est également accéssible depuis notre siège pour y déposer notre matériel lorsque la tablette est relevée.


![](./images/final/angle.jpg)

### **Hauteurs**

L'ajustement des hauteurs était un des points cruciaux de l'objet, comme expliqué plus haut j'ai rendu cette table accessible à la fois aux enfants et aux adultes, pour qu'ainsi l'objet soit le plus utile et utilisable possible. Les différentes hauteurs correspondent à la fois aux différentes hauteurs de table pour les différentes tranches d'age mais également aux différentes positions pour une personne adulte : 44, 49, 54, 59, 64, 69, 74, 80 cm. Le système devait par conséquent être facilement utilisable pour pouvoir être ajusté sur l'instant et par un enfant. J'ai donc opté pour un système coulissant.

![](./images/final/rails.jpg)

## Mise à jour
J'ai reproduit l'objet à échelle réduite, il est assez concluant. Il y a juste un bug au niveau du système des hauteurs, la table se soulève car elle est trop légère donc on a dû mal à changer la hauteur juste avec les mains, cependant, ce problème se règle en alourdissant la partie du bas pour éviter qu'elle ne se soulève 

![](./images/final/photo4.jpg)

J'ai donc testé les raccords avec le système de pince à taille réelle et elle n'a pas été très efficace dû à la longueur très courtes des pièces. Par manque de temps, la shaper ayant été occupée tout le lundi et mardi, je n'ai pas pu tester énormement de chose différentes, j'ai donc opté pour un système de tourillon partout dans la structure, pour garder une simplicité de démontage et une cohérence.

J'ai également eu un souci de matière, je n'ai pas eu celle que je voulais dans la dimension voulu, le magasin m'avait renvoyé à une autre ville, j'ai donc des épaisseurs plus petites que prévues, et je n'ai donc **pas pu alourdir** les pieds.

# Résultat 

###  3D

![](./images/final/imagetable.jpg)


## Nom et pictogramme

Ensuite, il a fallu trouver un nom pour cet objet : **Sit'up**. 
L'objet est étroitement lié aux différentes positions assises, donc tout naturelement *Sit*, mais il fallait quelque chose pour décire le mouvement, j'ai donc opté pour *Up*, et non *down* comme le voudrait le  verbe en anglais, car un enfant peut l'utiliser debout et cela renvoie également à  l'action de se relever une fois assis et se mouvoir dans les différentes positions.

## Matériaux 

- Planche en bois multiplex 8 mm 
- Planche en bois multiplex 12 mm 
- Planche légère 3mm 430x567 mm
- 4 Charnières + vis 
- Tourillons 6 mm

Un total pour moins de 50 euros 


## Modèle 2D and 3D 


- [Fichier pour découpe .svg (planche 12 mm)](./files/planche1.svg)
- [Fichier pour découpe .svg (planche 8 mm)](./files/planche2.svg)
- [3D .stl](./files/table3D.stl)

## Guide d'assemblage et d'utilisation 



Voici un guide pour découper et monter la table.

### étape 1 : Shaper 
 
Si vous utilisez la shaper voici comment procéder pour la découpe. Tout d'abord il vous faut vous munir de 2 planches en bois multiplex d'une de 12 mm et l'autre de 8 mm, vous trouvez ci-dessus 2 fichiers SVG destinés à cette impression. Il y un code couleur, les lignes : *noires* découpe de 12 mm, *brunes* 8mm, *vertes* découpe profondeur de 8mm. Il faut tout d'abord faire les découpes avec une fraise de 8 mm ou 6 mm et puis faire une dernière passe avec une fraise 3 mm. Pour savoir comment utiliser la shaper je vous renvoi vers mon [Git](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/ninwa.sansak/FabZero-Modules/module05/) et vers une [vidéo d'utilisation](https://www.youtube.com/watch?v=DekAjAOIVvQ&ab_channel=Shaper). 
 /!\ *Bien faire attention à choisir les découpes intérieur ou extérieures qui conviennent.*


![](./images/final/planche1.0.jpg)

![](./images/final/planche2.jpg)


### étape 2 :  assemblage

![](./images/final/photo1.jpg)

Maintenant que vous avez toutes vos pièces en bois, il suffit de les assembler.
Commençons par les pieds, pièces nécessaires :  3, 4, 5, 6, 7, 12, 13, 14, 15 et tourillons.

Prendre la pièce 3 et 5 et les superposer là où les découpage se superposent, et les joindre avec les pièces 13, 14 et les tourillons. Il faut donc faire les trous nécessaires pour accueillir les tourillons, les pièces 13 à l'avant, et la 15 à l'arrière, cette étape n'a pas besoin d'utiliser la shaper. Repéter l'opération avec l'autre pied de table.

![](./images/final/photo5.jpg)

Une fois les 2 parties faites les *joindre* à l'aide de la pièce 7, il faut donc placer les tourillons nécessaire de part et d'autres des pieds 

![](./images/final/photo6.jpg)

La partie du bas est faite, occupons nous de la partie de haut. 
Tout d'abord les jonctions avec la tablette. Prenez la pièce  8, 11, 16 ainsi que les charnières. 
Au dos de la pièce 16, à 23 cm du bord de 60 cm et à approximativement  1/3 des bords de 80 cm, visser les charnières. Ensuite sur l'autre partie de la charnière y fixer la pièce  11 /!\ *les deux pièces doivent être bien centrées l'une à l'autre.*

![](./images/final/photo7.jpg)

 Puis à nouveau à 5 cm du bord de 60 cm visser les 2 autres charnières à même distance /!\ *le coté fixe vers le coté  court.* Puis fixer l'autre partie des charnières à la pièce 8 /!\ *les deux pièces doivent être bien centrées l'une à l'autre.*

![](./images/final/photo8.jpg)

Puis prendre la pièce 8 et l'assembler aux pièce 1 et 2 de part et d'autre au niveau des trous du haut, en veillant bien à avoir la fine ligne découpée vers l'intérieur. Puis dans ajouter la pièce 9 à l'extrémité arrière. Dans les encoches prévues à cet effet, faire glisser la planche de 2 mm.

![](./images/final/photo9.jpg)
![](./images/final/photo10.jpg)
![](./images/final/photo11.jpg)
Pour finir il suffit de rassembler les 2 parties ensemble, 
pour se faire munissez vous de la pièce 10. Faites glisser les pièces verticales de la partie du haut dans les 2 interstices qui sont entre les 2 parties d'un pieds, et faites en sortes que le trou soit placé au niveau d'une encoche, puis insérer une des exrémités de la planche 10, il faut que l'extrémité traverse les 2 planches pour venir se coincer dans la 3e. Répéter l'opération de l'autre côté.

![](./images/final/photo3.jpg)

La table est désormais montée, il suffit maintenant de l'ajuster à votre hauteur.


![](./images/final/photo2.jpg)











