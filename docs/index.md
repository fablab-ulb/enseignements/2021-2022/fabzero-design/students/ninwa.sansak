## About me
Bonjour !

Je m'appelle **Sarah** et suis actuellement en master 2 en Architecture à la Cambre Horta. J'ai  choisi l'option **Architecture et Design** car elle nous forme à l'utilisation de logiciels et d'outils tels que l'imprimante 3D, la découpeuse laser etc. Des compétences qui s'avèrent toujours utiles dans la vie d'un jeune architecte


![](images/profil.jpg)


## Parcours de vie

Je suis née et j'ai grandi ici à Bruxelles, j'ai toujours été intéressée par tout ce qui touche de près ou de loin à l'art et la culture, l'architecture en fait bien-sûr partie. C'est pourquoi l'objet que je vais vous présenter ci-dessous touche à ce domaine.

## A propos de mon choix

Dans ma chambre j'ai une petite table sur laquelle je fais un peu tout, manger, travailler, stockage, mais surtout, peindre. J'aime beaucoup peindre et c'est sur celle-ci que j'y passe le plus clair de mon temps. Elle a un côté pratique où ce sont 2 tables qui s'emboitent ce qui permet de l'agrandir au besoin. Cependant, elle n'est pas idéale pour peindre, trop basse, pas assez modulable etc.

![](images/table.jpg)


### Objet 

C'est donc dans cette optique que je souhaite concevoir une petite table idéale pour peindre et dessiner qui est ajustable selon les besoins, mais également utilisable au quotidien.



